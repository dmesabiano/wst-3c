

<!DOCTYPE html>
<html lang="en">
  <head>
  @include("admin.admincss")
  </head>
  
  <div style="position: relative; left: 85%">
  <x-app-layout>

</x-app-layout>  
</div>
<style>

table,tr {
  
  border-radius: 15px;
}
th{
 
  border-collapse: collapse;
  background-color: #7ba0db;

}

tr:hover {background-color: #7ba0db;}

a:hover{color:black}
</style>

  <body>

  <div class="container-scroller">
  @include("admin.navbar")

<div style="position: relative; top: 60 px;right: -150px">
  <p style="font-size: 25px;padding-bottom:20px;">Products </p>

    <p style="font-size: 20px;padding-bottom:20px;">Here you can add new products and delete products from the list.</p>

    
<form action="{{url('/uploadchef')}}" method="Post" enctype="multipart/form-data" style="margin-left: 20%;margin-top: 5%;">
    

    @csrf

    <div style="padding-bottom:15px;">
        <label>Product Name</label>
        <input style="color:blue;" type="text" name="name" required="" placeholder="Product Name">
    </div>

    <div style="padding-bottom:15px;">
        <label>Type of Meat</label>
        <input style="color:blue;" type="text" name="specialty" required="" placeholder="Meat Type">
    </div>

    <div style="padding-bottom:15px;">
            <label >Price</label>
            <input style="color:black" type="num" name="price" placeholder="price" required>
        </div>

    <div>
        
        <input  type="file" name="image" required="">
    </div>
    <div style="margin-left:80px;padding-top:15px;">
            <input style="background-color:#3483eb;color:white;padding: 8px;" type="submit" value="Add New Product">
            <input style="background-color:#28bd57;color:white;padding: 8px;" type="reset" value="Reset the form">
        </div>

  </form>

  <br>
  <br>
  <br>


<table bgcolor="white" style="color:black;border-color:black;">
    <tr align="center" >
        <th style="padding: 30px;width: 150px;">Products's Name</th>
        <th style="padding: 30px;">Kind of Meat</th>
        <th style="padding: 30px;">Price</th>
        <th style="padding: 30px;">Image</th>
        <th style="padding: 30px;" colspan="3">Action</th>
    </tr>

    @foreach($data as $data)

    <tr align="center">

          <td style="width: 100px;padding: 20px;">{{$data->name}}</td>
          <td style="width: 100px;padding: 20px;">{{$data->specialty}}</td>
          <td style="width: 100px;padding: 20px;">{{$data->price}}</td>
          <td style="width: 70px;padding: 20px;"><img src="/chefimage/{{$data->image}}"></td>

          <td style="color:white;padding: 20px;">
          <a style="border: 3px solid red;background-color: red;border-radius: 10px" 
          href="{{url('/deletechef',$data->id)}}">Delete</a></td>

          <td style="color:white;padding: 20px;"><a style="border: 3px solid green;background-color: green;
  border-radius: 10px" href="{{url('/updateproduct',$data->id)}}">Update</a></td>
        </tr>

    @endforeach

</table>

</div>  
    <br>
  <br>
  <br>
  </div>
  <br>
  <br>
  <br>

  @include("admin.adminscript") 
  </body>
</html>