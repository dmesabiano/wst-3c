

<!DOCTYPE html>
<html lang="en">
  <head>

  <base href="/public">


  @include("admin.admincss")
  </head>
  
  <div style="position: relative; left: 85%">
  <x-app-layout>

</x-app-layout>  
</div>

  <body>

  <div class="container-scroller">
  @include("admin.navbar")


  <div style="position: relative; top: 60 px;right: -150px">

    <form action="{{url('/update',$data->id)}}" method="post" enctype="multipart/form-data">

    @csrf

    <p style="font-size: 25px;padding-bottom:20px;">Update Menu</p>

    <p style="font-size: 20px;padding-bottom:20px;">Here you can edit or update  your food menu for Weekly Specials</p>
    

        <div style="padding-bottom:15px;">
            <label >Title</label>
            <input style="color:black" type="text" name="title" value="{{$data->title}}" required>
        </div>

        <div style="padding-bottom:15px;">
            <label >Price</label>
            <input style="color:black" type="num" name="price" value="{{$data->price}}" required>
        </div>

        <div style="padding-bottom:15px;">
            <label >Description</label>
            <input style="color:black;" type="text" name="description" value="{{$data->description}}" required>
        </div>

        <div style="padding-bottom:15px;">
            <label >Current Image</label>
            <img height="200" width="200" src="/foodimage/{{$data->image}}">
        </div>

        <div>
            <label >New Image</label>
            <input style="color:white" type="file" name="image">
        </div>

        <div style="margin-left:80px;padding-top:15px;">
            <input style="background-color:#3483eb;color:white;padding: 8px;" type="submit" value="Update">
            <input style="background-color:#28bd57;color:white;padding: 8px;" type="submit" value="Cancel">
        </div>


    </form>




  
  </div>

  

  @include("admin.adminscript") 
  </body>
</html>