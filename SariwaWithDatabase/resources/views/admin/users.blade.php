



<!DOCTYPE html>
<html lang="en">

  <head>
  @include("admin.admincss")
  </head>
  
  <div style="position: relative; left: 85%">
  <x-app-layout>

</x-app-layout>  
</div>
<style>

table,tr {
  
  border-radius: 15px;
}
th{
 
  border-collapse: collapse;
  background-color: #7ba0db;

}

tr:hover {background-color: #7ba0db;}

a:hover{color:black}
</style>

  <body>
  <div class="container-scroller">

    @include("admin.navbar")
    <div style="position: relative; top: 60 px;right: -150px">
  <p style="font-size: 25px;padding-bottom:20px;">Users </p>

    <p style="font-size: 20px;padding-bottom:20px;">Here you can add new user accounts and update user details from the list.</p>

    <form action="{{url('/uploaduser')}}" method="post" enctype="multipart/form-data" style="margin-left: 20%;margin-top: 5%;">

    @csrf 

        <div style="padding-bottom:15px;">
            <label >Name</label>
            <input style="color:black" type="text" name="name" placeholder="Full Name" required>
        </div >

        <div style="padding-bottom:15px;">
            <label >Email</label>
            <input style="color:black" type="num" name="email" placeholder="Email Address" required>
        </div>

        <!-- <div style="padding-bottom:15px;">
            <label >Position</label>
            <input style="color:black" type="text" name="position" placeholder="position" required>
        </div> -->
        <div style="padding-bottom:15px;">
            <label >Password</label>
            <input style="color:black" type="text" name="password" placeholder="password" required>
        </div>



        <div style="margin-left:80px;padding-top:15px;">
            <input style="background-color:#3483eb;color:white;padding: 8px;" type="submit" value="Add New Account">
            <input style="background-color:#28bd57;color:white;padding: 8px;" type="reset" value="Reset the form">
        </div>


    </form>
  <br>
  <br>


    <table bgcolor="white" style="color:black;border-color:black;" >
            <tr >
                <th style="padding-top: 30px;padding-bottom: 30px;padding-left: 60px;padding-right: 60px">Name</th>
                <th style="padding-top: 30px;padding-bottom: 30px;padding-left: 60px;padding-right: 60px">Email</th>
                <!-- <th style="padding-top: 30px;padding-bottom: 30px;padding-left: 60px;padding-right: 60px">Position</th> -->
              
                <th style="padding-top: 30px;padding-bottom: 30px;padding-left: 60px;padding-right: 60px">Action</th>
            </tr>

            @foreach($data as $data)
            <tr align="left">
                <td style="width: 100px;padding: 20px;">{{$data->name}}</td>
                <td style="width: 100px;padding: 20px;">{{$data->email}}</td>
                <!-- <td style="width: 100px;padding: 20px;">{{$data->position}}</td> -->
                

                @if($data -> usertype == "1")
                <td align="center"><a style="border: 3px solid red;background-color: red;border-radius: 10px;color: white;"  href="{{url('/deleteuser',$data->id)}}">Delete</a></td>
                @else
                <td>Admin</td>

                @endif
            </tr>

            @endforeach




        </table>
    </div>

  </div>
  <br>
  <br>
  <br>

  @include("admin.adminscript") 
  </body>
</html>