<ul>
    @foreach($categoryRoot as $item )
    @if ($category->slug == $item->slug)
        {{$item->title}}
    @else
    <a href="{{route('Mrben.posts.category', ['slug' => $item->slug])}}">
        {{$item->title}}
    @endif
    @if($item->descendants)
    @include('Mrben.sub-categories',[
        'categoryRoot' => $item->descendants,
        'category' => $category
        ])
    @endif
    @endforeach

</ul>