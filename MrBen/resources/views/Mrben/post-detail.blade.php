@extends('layouts.mrben')

@section('title')
{{ $post-> title }}
@endsection

@section('description')
{{ $post-> description }}
@endsection
<h2 class="my-3">
  
</h2>

@section('content')
   <h2 class="mt-4 mb-3">
     {{ $post-> title }}
   </h2>
   {{Breadcrumbs::render('mrben_post', $post-> title)}}
   <div class="row">
      <!-- Post Content Column:start -->
      <div class="col-lg-8">
         <!-- thumbnail:start -->
         @if(file_exists(public_path($post->thumbnail)))
            <img class="card-img-top" src="{{ asset($post->thumbnail) }}" alt="{{ $post->title }}">
        @else
            <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="{{ $post->title }}">
        @endif
         <hr>
         <div>
         {!!$post->content!!}
         </div>
         <hr>
      </div>
      <div class="col-md-4">
         <!-- Categories Widget -->
         <div class="card mb-3">
            <h5 class="card-header">
               {{trans('Mrben.widget.categories')}}
            </h5>
            <div class="card-body">
               @foreach ($post->categories as $category)
               <a href="{{route('Mrben.posts.category',['slug' => $category->slug])}}" class="badge badge-primary py-2 px-4 my-1">
                  {{$category ->title}}
               </a>  
               @endforeach
            </div>
         </div>

         <!-- Side Widget tags:start -->
         <div class="card mb-3">
            <h5 class="card-header">
            {{trans('Mrben.widget.tags')}}
            </h5>
            <div class="card-body">
               <!-- tag list:start -->
               @foreach ($post->tags as $tag)
               <a href="{{route('Mrben.posts.tag',['slug' => $tag->slug])}}" 
               class="badge badge-info py-2 px-4 my-1">
                  #{{$tag->title}}
               </a>
               @endforeach
               <!-- tag list:end -->
            </div>
         </div>
         <!-- Side Widget tags:start -->
      </div>
      <!-- Sidebar Widgets Column:end -->
   </div>
@endsection