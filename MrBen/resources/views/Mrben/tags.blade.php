@extends('layouts.mrben')

@section('title')
    {{trans('mrben.title.tags')}}
@endsection

@section('content')
<h2 class="my-3">
  
</h2>
{{Breadcrumbs::render('mrben_tags')}}
   <div class="row">
      <div class="col">
        @forelse ($tags as $tag)
        <a href="{{route('Mrben.posts.tag',['slug' => $tag->slug])}}"
        class="badge badge-info mx-2 my-2 py-3 px-5">
               #{{$tag->title}}</a>
        @empty
        <h3 class="text-center">
              {{trans('Mrben.no_data.tags')}}
            </h3>

        @endforelse

      </div>
   </div>
   @if($tags -> hasPages())
<div class="row">
   <div class="col">
        {{$tags->links('vendor.pagination.bootstrap-4')}}
   </div>
</div>
    @endif

@endsection