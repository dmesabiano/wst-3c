@extends('layouts.mrben')

@section('title')
    {{ request()->get('keyword') }}
@endsection

@section('content')
<h2 class="my-3">
   <!-- {{trans('mrben.title.home')}} -->
</h2>

{{Breadcrumbs::render('mrben_search', request()->get('keyword')) }}


<div class="row">
   <div class="col">
      <!-- Post list:start -->
      @forelse ($posts as $post)
      <div class="card mb-4">
         <div class="card-body">
            <div class="row">
               <div class="col-lg-6">
                  <!-- thumbnail:start -->
                  @if(file_exists(public_path($post->thumbnail)))
                  <img class="card-img-top" src="{{ asset($post->thumbnail) }}" alt="{{ $post->title }}">
                  @else
                  <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="{{ $post->title }}">
                  <!-- thumbnail:end -->
                  @endif
               </div>
               <div class="col-lg-6">
                  <h2 class="card-title">
                    {{$post->title}}
                  </h2>
                  <p class="card-text">
                    {{$post->description}}
                  </p>
                  <a href="{{route('Mrben.posts.detail',['slug' => $post->slug])}}" class="btn btn-primary">
                     {{trans('Mrben.button.read_more.value')}}
                  </a>
               </div>
            </div>
         </div>
      </div>
      @empty
        <!-- empty -->
      <h3 class="text-center">
        {{trans('Mrben.no_data.search_posts')}}
      </h3>
      @endforelse
      <!-- Post list:end -->
   </div>
</div>
    @if ($posts->hasPages())
    <div class="row">
        <div class="col">
         {{$posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
    @endif
@endsection