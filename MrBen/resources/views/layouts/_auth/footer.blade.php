<footer class="py-4 bg-light mt-auto">
   <div class="container-fluid">
      <div class="d-flex align-items-center justify-content-between small">
         <div class="text-muted">Copyright © My End-Term Requirement 2022 || {{ config('app.name')}}</div>
         <div>
            <!-- <a href="#">Privacy Policy</a>
            ·
            <a href="#">Terms &amp; Conditions</a> -->
         </div>
      </div>
   </div>
</footer>