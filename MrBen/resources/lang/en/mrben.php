<?php
/*
language : English
*/
return [
    'title' => [
        'home' => "Home",
        'search' => "Search: :keyword",
        'categories' => "Categories",
        'category' => "Category: :title",
        'tags' => "Tags",
        'tag' => "Tag: :title",
    ],
    'form_control' => [
        'input' => [
            'search' => [
                'label' => 'Search',
                'placeholder' => 'Search for categories',
                'attribute' => 'search'
            ]
        ],
    ],
    'menu' => [
        'home' => "Home",
        'categories' => "Categories",
        'tags' => "Breed",
        'dashboard' => 'Dashboard',
        'login' => "Login",
        'logout' => "logout",
        'message' => "Inquire"
    ],
    'button' => [
        'read_more' => [
            'value' => "View more"
        ]
    ],
    'widget' => [
        'categories' => 'Categories',
        'tags' => "Breed"
    ],
    'no_data' => [
        'tags' => "Tag data does not exist yet",
        'posts' => "Post data does not exist yet",
        'categories' => "No category data yet",
        'search_posts' => 'Post search data not found'
    ]
];
