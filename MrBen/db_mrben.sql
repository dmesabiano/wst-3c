-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2022 at 08:49 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mrben`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `thumbnail`, `description`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Purecali', 'purecali', '/storage/photos/1/categories/hyy.jpg', 'The Californian rabbit breed is distinguished by\r\n             its coloring--a white body with black points the legs, nose and ears.', NULL, '2022-06-14 06:17:55', '2022-06-15 23:31:49'),
(3, 'Transylvania Giant rabbit', 'transylvania-giant-rabbit-3', '/storage/photos/1/categories/Transylvania.jpg', 'The Transylvanian Giant Rabbit is a new breed \r\n            in Romanian cuniculture characterized by a normal weight of about 6.0 kg.', NULL, '2022-06-14 06:17:55', '2022-06-15 18:00:22'),
(4, 'Flemish Giant', 'flemish-giant-4', '/storage/photos/1/categories/FG.jpg', 'The Flemish Giant is generally a docile breed, \r\n            and these giant pet rabbits make good companions and loving family pets.', NULL, '2022-06-14 06:17:55', '2022-06-15 18:01:56'),
(13, 'Pure new zealand', 'pure-new-zealand-3', '/storage/photos/1/categories/purenewzealand.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 22:36:11', '2022-06-15 22:36:11'),
(14, 'Purecali2', 'purecali2-4', '/storage/photos/shares/minirex.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 22:44:23', '2022-06-15 22:44:23'),
(15, 'F1cali', 'f1cali-14', '/storage/photos/1/categories/hollandlop.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 22:53:58', '2022-06-15 22:53:58'),
(16, 'harley quin', 'harley-quin-4', '/storage/photos/1/categories/hyy.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 22:56:40', '2022-06-15 22:56:40'),
(17, 'sampletitle', 'sampletitle-4', '/storage/photos/1/categories/FG.jpg', 'sample only1', NULL, '2022-06-15 23:14:06', '2022-06-15 23:16:28'),
(18, 'samplesample', 'samplesample-1', '/storage/photos/1/categories/Transylvania.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 23:53:56', '2022-06-15 23:53:56'),
(19, 'rabbit11', 'rabbit11-4', '/storage/photos/1/categories/qwerty.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 23:58:15', '2022-06-15 23:58:15'),
(20, 'rabbit111', 'rabbit111-17', '/storage/photos/1/categories/hollandlop.jpg', 'Its color pattern is Himalayan, several shades of black for the fur at the extremities being observed.', NULL, '2022-06-15 23:59:05', '2022-06-15 23:59:05'),
(21, 'Holland', 'holland-14', '/storage/photos/1/categories/hollandlop.jpg', 'This tiny bunny is the essence of cuteness due to their cuddly size and floppy ears. Though there are different breeds of lops, the Holland Lop is the smallest of them all.', NULL, '2022-06-16 13:45:11', '2022-06-16 13:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`id`, `category_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2022-06-15 01:39:44', '2022-06-15 01:39:44'),
(5, 3, 4, '2022-06-15 15:39:36', '2022-06-15 15:39:36'),
(6, 4, 5, '2022-06-15 15:43:28', '2022-06-15 15:43:28'),
(13, 1, 12, '2022-06-15 17:18:54', '2022-06-15 17:18:54'),
(14, 13, 13, '2022-06-16 13:07:16', '2022-06-16 13:07:16'),
(15, 1, 14, '2022-06-16 13:48:51', '2022-06-16 13:48:51'),
(16, 21, 14, '2022-06-16 13:48:51', '2022-06-16 13:48:51'),
(17, 21, 11, '2022-06-16 13:51:23', '2022-06-16 13:51:23'),
(18, 13, 3, '2022-06-21 18:20:50', '2022-06-21 18:20:50'),
(19, 3, 8, '2022-06-21 18:40:58', '2022-06-21 18:40:58'),
(20, 4, 8, '2022-06-21 18:40:58', '2022-06-21 18:40:58'),
(21, 21, 9, '2022-06-21 18:42:18', '2022-06-21 18:42:18'),
(22, 21, 7, '2022-06-21 18:45:16', '2022-06-21 18:45:16'),
(23, 16, 6, '2022-06-21 18:46:55', '2022-06-21 18:46:55'),
(24, 21, 6, '2022-06-21 18:46:55', '2022-06-21 18:46:55');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_06_08_201059_create_categories_table', 1),
(6, '2022_06_12_230615_create_tags_table', 1),
(7, '2022_06_13_092542_create_posts_table', 1),
(8, '2022_06_13_093413_create_category_post_table', 1),
(9, '2022_06_13_093528_create_post_tag_table', 1),
(10, '2022_06_14_095350_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 3),
(3, 'App\\Models\\User', 2),
(3, 'App\\Models\\User', 5),
(13, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'post_show', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(2, 'post_create', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(3, 'post_update', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(4, 'post_detail', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(5, 'post_delete', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(6, 'category_show', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(7, 'category_create', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(8, 'category_update', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(9, 'category_detail', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(10, 'category_delete', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(11, 'tag_show', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(12, 'tag_create', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(13, 'tag_update', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(14, 'tag_delete', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(15, 'role_show', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(16, 'role_create', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(17, 'role_update', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(18, 'role_detail', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(19, 'role_delete', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(20, 'user_show', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(21, 'user_create', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(22, 'user_update', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(23, 'user_detail', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(24, 'user_delete', 'web', '2022-06-14 06:17:55', '2022-06-14 06:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(240) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('publish','draft') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `thumbnail`, `description`, `content`, `status`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Purecali', 'purecali', '/storage/photos/1/categories/purecali.jpg', 'The Californian rabbit breed', '<p><em>Price</em>: PHP 5,000.</p>\r\n<p>The Californian rabbit breed is distinguished by its coloring a white body with black points the legs, nose and ears. This color pattern with black markings is caused by the Himalayan gene</p>', 'publish', '2022-06-15 01:39:44', '2022-06-21 18:25:53', 1),
(3, 'Pure New Zealand', 'pure-new-zealand', '/storage/photos/1/categories/purenewzealand.jpg', 'The New Zealand', '<p>Price: PHP 5,000 - 10,000</p>\r\n<p>The New Zealand&nbsp;a breed of rabbit, which despite the name, is American in origin. The breed originated in California, possibly from rabbits imported from New Zealand.</p>', 'publish', '2022-06-15 15:38:16', '2022-06-21 18:49:58', 1),
(4, 'Transylvania Giant Rabbit', 'transylvania-giant-rabbit', '/storage/photos/1/categories/Transylvania.jpg', 'Giant Breed', '<p>Price: PHP 100,000 - 200, 000.</p>\r\n<p>The Transylvanian Giant Rabbit is a new breed in Romanian cuniculture characterized by a normal weight of about 6.0 kg with limits between 4.5 and 9.0 kg.</p>', 'publish', '2022-06-15 15:39:36', '2022-06-21 18:51:00', 1),
(5, 'Flemish Giant', 'flemish-giant', '/storage/photos/1/categories/FG.jpg', 'Giant Rabbit', '<p>Price: PHP 20,000 - 30,000.</p>\r\n<p>Flemish Giants are also known as Gentle Giants mainly because of their docile nature. These pet rabbits make excellent companions and adorable family pets.</p>', 'publish', '2022-06-15 15:43:27', '2022-06-21 18:49:02', 1),
(6, 'Mini Rex', 'mini-rex', '/storage/photos/1/categories/minirex.jpg', 'Dwarf Rabbit', '<p>Price: PHP 5,000 - 10,000.</p>\r\n<p>Mini Rex are always happy to have attention, making them delightful companions and pets. Their pleasant personalities and small size make them the ideal candidate for a house pet.</p>', 'publish', '2022-06-15 15:45:38', '2022-06-21 18:46:55', 1),
(7, 'Holland Lop', 'holland-lop', '/storage/photos/1/categories/hollandlop.jpg', 'Fancy Rabbit', '<p>Price: PHP 5,000-7,000.</p>\r\n<p>Holland Lops are very intelligent and are always finding new ways to escape. Once they ate through the chicken wire surrounding their house and then ran away! We found them in our neighbor\'s yard eating clovers. Rabbits love to dig.</p>', 'publish', '2022-06-15 15:46:58', '2022-06-21 18:45:16', 1),
(8, 'Hyla Optima', 'hyla-optima', '/storage/photos/1/categories/hyy.jpg', 'The PS Hyla OPTIMA', '<p>Price: PHP 20,000.</p>\r\n<p>The result of cross-breeding between the GP OPTIMA female and the NGPC male from our new selection model based on sustainability.</p>\r\n<p>&nbsp;</p>', 'publish', '2022-06-15 17:09:27', '2022-06-21 18:40:58', 1),
(9, 'Dwarf Rabbit', 'dwarf-rabbit', '/storage/photos/1/categories/qwerty.jpg', 'Fancy Rabbit', '<p>Price: PHP 5,000.</p>\r\n<p>If you like cute, cuddly, and petite, then yes, Netherland Dwarf rabbits make good pets. This breed is a bit more skittish than other domestic rabbit breeds. They are also a lot smaller, so more delicate.</p>', 'publish', '2022-06-15 17:11:17', '2022-06-21 18:42:18', 1),
(10, 'mini rex 3', 'mini-rex-3', '/storage/photos/1/categories/qw.jpg', 'If you like cute, cuddly, and petite, then yes, Netherland Dwarf rabbits make good pets. This breed is a bit more skittish than other domestic rabbit breeds. They are also a lot smaller, so more delicate.', '<h2>Holland Varaint</h2>', 'draft', '2022-06-15 17:12:52', '2022-06-15 17:12:52', 1),
(11, 'f1 Holland Lop', 'f1-holland-lop', '/storage/photos/1/categories/qqq.jpg', 'Upgraded Fancy rabbit', '<p>Price: PHP 3,000.</p>\r\n<p>If you like cute, cuddly, and petite, then yes, Netherland Dwarf rabbits make good pets. This breed is a bit more skittish than other domestic rabbit breeds. They are also a lot smaller, so more delicate.</p>', 'publish', '2022-06-15 17:14:36', '2022-06-21 18:43:45', 1),
(12, 'F1cali', 'f1cali', '/storage/photos/1/categories/purecali.jpg', 'Crossed to different breed.', '<p>Price: PHP 1,000 - 3,000</p>\r\n<p>F1 denotes breeding a purebred to another breed. The F1 specimen must have basic manifestation of the intended breed. F2 denotes breeding a F1 to a purebred or another hybrid that is F1 or F2. F3 denotes breeding a F2 to a purebred or another hybrid that is F2 or F3.</p>', 'publish', '2022-06-15 17:18:54', '2022-06-21 18:38:37', 1),
(13, 'F1NZ', 'f1nz', '/storage/photos/1/categories/purenewzealand.jpg', 'Purebred to another breed.', '<p>Price: PHP 1,000 - 3,000</p>\r\n<p>F1 denotes breeding a purebred to another breed. The F1 specimen must have basic manifestation of the intended breed. F2 denotes breeding a F1 to a purebred or another hybrid that is F1 or F2.&nbsp;</p>', 'publish', '2022-06-16 13:07:16', '2022-06-21 18:35:39', 1),
(14, 'G1Holland Lop', 'g1holland-lop', '/storage/photos/1/categories/qw.jpg', 'This tiny bunny is the essence of cuteness due to their cuddly size and floppy ears', '<p><em>Price: PHP 3,000 - 5,000.</em></p>\r\n<p><em>Holland Lops</em> are one of the most popular rabbit breeds in the United States and the United Kingdom.Though there are different breeds of lops, the Holland Lop is the smallest of them all.</p>', 'publish', '2022-06-16 13:48:51', '2022-06-21 18:32:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(3, 3, 2, '2022-06-15 15:38:16', '2022-06-15 15:38:16'),
(4, 4, 3, '2022-06-15 15:39:36', '2022-06-15 15:39:36'),
(5, 1, 1, '2022-06-15 15:40:49', '2022-06-15 15:40:49'),
(6, 5, 4, '2022-06-15 15:43:27', '2022-06-15 15:43:27'),
(7, 6, 5, '2022-06-15 15:45:38', '2022-06-15 15:45:38'),
(8, 7, 6, '2022-06-15 15:46:58', '2022-06-15 15:46:58'),
(9, 8, 7, '2022-06-15 17:09:27', '2022-06-15 17:09:27'),
(10, 9, 5, '2022-06-15 17:11:17', '2022-06-15 17:11:17'),
(11, 10, 6, '2022-06-15 17:12:52', '2022-06-15 17:12:52'),
(12, 11, 6, '2022-06-15 17:14:36', '2022-06-15 17:14:36'),
(13, 12, 1, '2022-06-15 17:18:54', '2022-06-15 17:18:54'),
(14, 13, 2, '2022-06-16 13:07:16', '2022-06-16 13:07:16'),
(15, 14, 6, '2022-06-16 13:48:51', '2022-06-16 13:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', 'web', '2022-06-14 06:17:56', '2022-06-14 06:17:56'),
(2, 'Admin', 'web', '2022-06-14 06:17:56', '2022-06-14 06:17:56'),
(3, 'Editor', 'web', '2022-06-14 06:17:56', '2022-06-14 06:17:56'),
(10, 'roger sample role', 'web', '2022-06-14 17:10:00', '2022-06-14 17:10:00'),
(13, 'Role test', 'web', '2022-06-15 04:44:06', '2022-06-15 04:44:06');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 10),
(1, 13),
(2, 1),
(2, 2),
(2, 3),
(2, 10),
(2, 13),
(3, 1),
(3, 2),
(3, 3),
(3, 13),
(4, 1),
(4, 2),
(4, 3),
(4, 13),
(5, 1),
(5, 2),
(5, 3),
(5, 13),
(6, 1),
(6, 2),
(6, 10),
(6, 13),
(7, 1),
(7, 2),
(7, 10),
(7, 13),
(8, 1),
(8, 2),
(8, 13),
(9, 1),
(9, 2),
(9, 13),
(10, 1),
(10, 2),
(10, 13),
(11, 1),
(11, 2),
(11, 10),
(11, 13),
(12, 1),
(12, 2),
(12, 10),
(12, 13),
(13, 1),
(13, 2),
(13, 13),
(14, 1),
(14, 2),
(14, 13),
(15, 1),
(15, 10),
(15, 13),
(16, 1),
(16, 10),
(16, 13),
(17, 1),
(17, 13),
(18, 1),
(18, 13),
(19, 1),
(19, 13),
(20, 1),
(20, 10),
(20, 13),
(21, 1),
(21, 10),
(21, 13),
(22, 1),
(22, 13),
(23, 1),
(23, 13),
(24, 1),
(24, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Purecali', 'purecali', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(2, 'Pure New Zealand', 'pure new zealanad', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(3, 'Transylvania Giant rabbit', 'transylvania giant rabbit', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(4, 'Flemish Giant', 'flemish giant', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(5, 'Mini Rex', 'mini rex', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(6, 'Holland Lop', 'holland lop', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(7, 'Hyla Optima', 'hyla-optima', '2022-06-15 17:08:22', '2022-06-15 17:08:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super admin', 'superadmin@gmail.com', '2022-06-14 06:17:55', '$2y$10$nCAs2r.zas99teFYJZDu9ue5ByFEaegojOUP.JZpHTLZnTU7pwc/u', 'HnBtwiLriR3pvXeREVjFnN0oCPBnCgxPO3ia6RUdrCE9pxLjFerbrtWukmtx', '2022-06-14 06:17:55', '2022-06-14 06:17:55'),
(2, 'Rogereditor', 'rogereditor@gmail.com', NULL, '$2y$10$uRJjiTJHW5oHpwz3ze/msOwfLHznvdt5s8pk54Tr1E3T4cURUoAku', NULL, '2022-06-15 02:10:58', '2022-06-15 02:10:58'),
(3, 'rabbitAdmin', 'rabbitAdmin@gmail.com', NULL, '$2y$10$hxDqzg9HrS23.1npwjlPQugEIR7hgbv55Hg3RBeL7IL./Hvozm2gS', NULL, '2022-06-15 03:11:41', '2022-06-15 03:11:41'),
(5, 'sample1', 'sample@example.test', NULL, '$2y$10$jO.rhEbQVZDgsKxwi9cD9.EGpbwTAs0PNXlY/IaT4TgtVMOLVPjf2', NULL, '2022-06-15 03:57:55', '2022-06-15 03:57:55'),
(6, 'rabbit1', 'rabbit1@mail.com', NULL, '$2y$10$pPi//vHue3nbu22Yas3rkO34Hzbc.z6HnEAIgXYUyxSCxPoRn6Rx6', NULL, '2022-06-15 04:45:36', '2022-06-15 04:45:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_post_category_id_foreign` (`category_id`),
  ADD KEY `category_post_post_id_foreign` (`post_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `category_post`
--
ALTER TABLE `category_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_post`
--
ALTER TABLE `category_post`
  ADD CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
