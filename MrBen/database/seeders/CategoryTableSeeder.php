<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
            'title' => 'Purecali',
            'slug' => 'purecali',
            'description' => 'The Californian rabbit breed is distinguished by
             its coloring--a white body with black points (the legs, nose and ears).',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => null
            ],
            [
            'title' => 'Pure New Zealand',
            'slug' => 'pure new zealand',
            'description' => 'The New Zealand is a breed of rabbit,
             which despite the name, is American in origin.',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => 1
            ],
            [
            'title' => 'Transylvania Giant rabbit',
            'slug' => 'transylvania giant rabbit',
            'description' => 'The Transylvanian Giant Rabbit is a new breed 
            in Romanian cuniculture characterized by a normal weight of about 6.0 kg.',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => 1
            ],
            [
            'title' => 'Flemish Giant',
            'slug' => 'flemish giant',
            'description' => 'The Flemish Giant is generally a docile breed, 
            and these giant pet rabbits make good companions and loving family pets.',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => null
            ],
            [
            'title' => 'Mini Rex',
            'slug' => 'mini rex',
            'description' => 'Mini rex rabbits are roughly 10 to 12 inches long 
            and weigh less than 5 pounds.',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => null
            ],
            [
            'title' => 'Holland Lop',
            'slug' => 'holland lop',
            'description' => 'This tiny bunny is the essence of cuteness due to their cuddly size and
             floppy ears.',
            'thumbnail' => 'noimage.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'parent_id' => null
            ],
            ]);
    }
}
