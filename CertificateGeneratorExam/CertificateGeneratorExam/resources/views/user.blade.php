<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
    <link rel="icon" href="/assets/fav.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/E_login.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
</head>
<body>

	<div class="container">
  <div class="row">
    
      <div class="col-md-6 mb-5">
      
      <!-- Login-->
      <div class="card shadow animated zoomIn slow p-5">
      <h3 class="text-center font-weight-bold text-uppercase mb-3">Login Here</h3>
      
      <form>
        <div class="form-group">
          <label>Enter Username</label>
          <input type="text" class="form-control">
        </div>
        <div class="form-group">
          <label>Enter Password</label>
          <input type="password" class="form-control">
        </div>
         
        <button type="submit" class="btn btn-outline-dark btn-block rounded-pill">Login</button>
        </form>
         <h6 class="mt-3">Don't have an account? <a href="{{url('sign_up')}}"> Create Account Here</a></h6>
   
      </div>
    </div>
  
  </div>
</div>

</body>
</html>