@extends('master')

@section('title')
    <title>Seminars</title>
@endsection

@section('content')


<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="topnav__link active">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="topnav__link">Admins</a>

    </li>
    <li class="topnav__item">
      <a href="/logout"><i class="fa fa-sign-out fa-2x text-danger"></i></a>
      
    </li>
    
  </ul>
</header>
<marquee behavior="" direction=""><span id='ct6' class="p-1" style="background-color: #FFBF00;"></span></marquee>


<div class="container">
@if(Session::has('success-seminar'))
<div class="alert alert-success">
    {{ Session::get('success-seminar') }}
    @php
        Session::forget('success-seminar');
    @endphp
</div>
@endif
</div>



<div class="card mb-3 col-sm-6 mx-auto mt-4">
{{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if(Session::has('success-seminar'))
  <div class="alert alert-success">
      {{ Session::get('success-seminar') }}
      @php
          Session::forget('success-seminar');
      @endphp
  </div>
@endif



<div class="card-body">
  <div class = "container col-sm-12">
  <form action="/seminars" method = "post" enctype="multipart/form-data">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <div class="form-row">
         

          <div class="input-group mb-3">
            <span class="input-group-text text-white" style="background-color: #082b54;">Title</span>
            <input type="text" name="title" class="form-control">
          </div>
          <div class="input-group">
              <span class="input-group-text text-white" style="background-color: #082b54;">Date</span>
              <input type="date" class="form-control" name="sdate"> 
              <span class="p-2 fw-bold">TO</span>
              <input type="date" class="form-control" name="edate">
              <span aria-describedby="dateHelp"></span>
              
          </div>
          <div id="dateHelp" class="form-text mb-3">The date must be the day after tomorrow</div>
          <div class="input-group mb-3">
              <span class="input-group-text text-white" style="background-color: #082b54;">Venue</span>
              <input type="text" name="venue" class="form-control">
          </div>


            <center>
              <button type="submit" class="btn btn-block mx-auto" style="background-color: #FFBF00">Add Seminar</button>
            </center>
        </div>
    </form>
</div>
</div>
</div>

<script>
$(function(){
      $("#seminarsTable").DataTable();
  });
</script>

@push('javascript-internal')
<script>
    $(document).ready(function() {
       //Event: delete
       $("form[role='alert']").submit(function(event) {
          event.preventDefault();
          Swal.fire({
          title: $(this).attr('alert-title'),
          text:  $(this).attr('alert-text'),
          icon: 'warning',
          allowOutsideClick: false,
          showCancelButton: true,
          cancelButtonText: $(this).attr('alert-btn-cancel'),
          reverseButtons: true,
          confirmButtonText: $(this).attr('alert-btn-yes'),
       }).then((result) => {
          if (result.isConfirmed) {
             //process ng deleting 
            event.target.submit();
           
          }
       });

       });

    });
    
</script>
@endpush


<div class="container">
<div class="row my-5">
  <div class="col-sm-12 mx-auto">
    <table class="table w-100 mx-auto" id="seminarsTable">
      <thead class="bg-dark text-white">
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Date</th>
          <th>Venue</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($seminars as $seminar)
        <tr>
          <td>{{$seminar->id}}</td>
          <td>{{$seminar->title}}</td>
          <td>{{$seminar->date}}</td>
          <td>{{$seminar->venue}}</td>
          <td>

            @if ($seminar->status == "1")
            <span class="text-success">Active</span>

            @else

            <span style="color: gray;">Inactive</span>
            @endif
            
          </td>
          <td>
              <form action="/seminars/delete/{{$seminar->id}}" role="alert" method="GET" 
              alert-title="{{trans('seminars.alert.delete.title')}}" alert-text="{{trans('seminars.alert.delete.message.confirm',['title' => $seminar->title])}}"
              alert-btn-cancel="{{trans('seminars.button.cancel.value')}}" alert-btn-yes="{{trans('seminars.button.delete.value')}}">
                <div class="text-center">
                  <a href="/seminars/edit/{{$seminar->id}}"  class="btn btn-sm btn-warning"
                    role="button"> <i  class="fas fa-edit"></i></a>
                  @csrf
                  <!-- @method('DELETE') -->
                  <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                  </button>
                </div>
              </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@include('sweetalert::alert')

@push('javascript-external')

@stack('javascript-external')
@stack('javascript-internal')



@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush


@endsection


