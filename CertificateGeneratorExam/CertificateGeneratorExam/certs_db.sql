-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2022 at 05:59 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certs_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `username`, `password`, `updated_at`, `created_at`) VALUES
(1, 'admin123@gmail.com', 'admin123', '0192023a7bbd73250516f069df18b500', NULL, NULL),
(5, 'benjiepecson@yahoo.com', 'benjie123', '4d9bd51e18bcd9b7fbbb2cb4e9b611f1', '2022-06-26 07:33:30', '2022-06-26 07:33:30'),
(6, 'benjiepecson@psu.edu.ph', 'benjie890', '6da01d7cee4ca34d2dd2e714b6de2b60', '2022-06-26 07:38:07', '2022-06-26 07:38:07');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `seminar_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `awardee` varchar(255) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `seminar_id`, `token`, `awardee`, `img_path`, `creator`, `created_at`, `updated_at`) VALUES
(1, 2, 'ad7444527186b5dbc5262b9ab4235c4d', 'Benjie Pecson', 'certificates/certificate_2113-06-37.png', 'admin123@gmail.com', '2022-06-21 05:06:37', '2022-06-21 05:06:37'),
(2, 3, '736cb9a98604d506dcb3ce725589e910', 'Benjie Pecson', 'certificates/certificate_2113-47-09.png', 'admin123@gmail.com', '2022-06-21 05:47:09', '2022-06-21 05:47:09'),
(3, 5, '3bad27d473bb53932fe69c22414702d1', 'Benjie Pecson', 'certificates/certificate_2114-23-48.png', 'admin123@gmail.com', '2022-06-21 06:23:48', '2022-06-21 06:23:48'),
(4, 5, '71a31004c8fb502ae4eeb3f23049ff7d', 'Louie Catabay', 'certificates/certificate_2114-24-12.png', 'admin123@gmail.com', '2022-06-21 06:24:12', '2022-06-21 06:24:12'),
(5, 5, '0466085990d5d4f8fef0dd17cc590cbb', 'Camila Delos Santos', 'certificates/certificate_2114-35-05.png', 'admin123@gmail.com', '2022-06-21 06:35:05', '2022-06-21 06:35:05'),
(6, 2, '6692b7ca4a4bde9a94351423837ebd69', 'Bnjpcson', 'certificates/certificate_2114-37-46.png', 'admin123@gmail.com', '2022-06-21 06:37:46', '2022-06-21 06:37:46'),
(7, 8, 'd805788235e3df175392c7746f0867f1', 'Benjie Pecson', 'certificates/certificate_2405-26-49.png', 'admin123@gmail.com', '2022-06-23 21:26:49', '2022-06-23 21:26:49'),
(8, 8, '819747ee066126519f96402fd7962eed', 'Katrina Urbano', 'certificates/certificate_2405-34-02.png', 'admin123@gmail.com', '2022-06-23 21:34:02', '2022-06-23 21:34:02'),
(9, 13, 'a4161f172e34a3f4fa0d1b842d9f1521', 'Benjie Pecson', 'certificates/certificate_2613-38-03.png', 'admin123@gmail.com', '2022-06-26 05:38:03', '2022-06-26 05:38:03'),
(10, 12, '6c091a1bcab9c111d9cfab2c39ead6c6', 'Benjie Pecson', 'certificates/certificate_2613-41-11.png', 'admin123@gmail.com', '2022-06-26 05:41:11', '2022-06-26 05:41:11'),
(11, 14, '7204f8b746cd70eba1577979f6625d8e', 'Benjie Pecson', 'certificates/certificate_2613-41-24.png', 'admin123@gmail.com', '2022-06-26 05:41:24', '2022-06-26 05:41:24'),
(12, 14, 'ddf7c5f70ad8f50e1d4f8bff53044863', 'Jonel Nagtalon', 'certificates/certificate_2615-44-46.png', 'admin123@gmail.com', '2022-06-26 07:44:46', '2022-06-26 07:44:46'),
(13, 12, '381ad847a7931d6e3096f69d7ab6aa65', 'Louie Catabay', 'certificates/certificate_2615-46-24.png', 'admin123@gmail.com', '2022-06-26 07:46:24', '2022-06-26 07:46:24'),
(14, 13, '2caf973b5cfa4d17472eb1b45fb039cd', 'Camila Delos Santos', 'certificates/certificate_2615-52-12.png', 'benjiepecson@yahoo.com', '2022-06-26 07:52:12', '2022-06-26 07:52:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_06_20_132553_create_seminar_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminars`
--

CREATE TABLE `seminars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `edate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seminars`
--

INSERT INTO `seminars` (`id`, `title`, `sdate`, `edate`, `venue`, `status`, `created_at`, `updated_at`) VALUES
(12, 'Developing Outstanding Leadership Skills.', '2022-06-28', '2022-06-30', 'Urdaneta City, Pangasinan', 1, '2022-06-26 02:20:50', '2022-06-26 02:20:50'),
(13, 'Developing Business Management Skills for Women.', '2022-06-30', '2022-07-01', 'Urdaneta City, Pangasinan', 1, '2022-06-26 02:34:08', '2022-06-26 03:27:11'),
(14, 'How To Make Lumpia', '2022-06-30', '2022-07-08', 'Urdaneta City, Pangasinan', 1, '2022-06-26 04:40:48', '2022-06-26 04:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `signature`
--

CREATE TABLE `signature` (
  `signature_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `e_sig` varchar(100) NOT NULL,
  `seminar_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `seminar_name` text NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `template` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `seminar_name`, `img_path`, `template`, `created_at`, `updated_at`) VALUES
(8, 'Developing Outstanding Leadership Skills.', 'templatesFolder/seminar_2612-35-35.png', 0, '2022-06-26 04:35:35', '2022-06-26 04:35:35'),
(9, 'Developing Business Management Skills for Women.', 'templatesFolder/seminar_2612-40-12.png', 1, '2022-06-26 04:40:12', '2022-06-26 04:40:12'),
(14, 'How To Make Lumpia', 'templatesFolder/seminar_2613-18-58.png', 1, '2022-06-26 05:18:58', '2022-06-26 05:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(15) NOT NULL,
  `password` varchar(12) NOT NULL,
  `event_title` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `seminars`
--
ALTER TABLE `seminars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`signature_id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seminars`
--
ALTER TABLE `seminars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `signature`
--
ALTER TABLE `signature`
  MODIFY `signature_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
