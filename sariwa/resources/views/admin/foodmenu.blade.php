
<html lang="en">
  <head>
  @include("admin.admincss")
  </head>
  
  <div style="position: relative; left: 85%">
  <x-app-layout>

</x-app-layout>  
</div>

<style>

table,tr {
  
  border-radius: 15px;
}
th{
 
  border-collapse: collapse;
  background-color: #7ba0db;

}

tr:hover {background-color: #7ba0db;}

a:hover{color:black}
</style>

    <body>

  <div class="container-scroller">
  @include("admin.navbar")

  <div style="position: relative; top: 60 px;right: -150px">

  <p style="font-size: 25px;padding-bottom:20px;">Food Menu</p>

    <p style="font-size: 20px;padding-bottom:20px;">Here you can Post your food menu for Weekly Specials</p>


    <form action="{{url('/uploadfood')}}" method="post" enctype="multipart/form-data" style="margin-left: 20%;margin-top: 5%;">

    @csrf 

    
        <div style="padding-bottom:15px;">
            <label >Title</label>
            <input style="color:black" type="text" name="title" placeholder="Name of food" required>
        </div >

        <div style="padding-bottom:15px;">
            <label >Price</label>
            <input style="color:black" type="num" name="price" placeholder="Price" required>
        </div>

        <div style="padding-bottom:15px;">
            <label >Image</label>
            <input style="color:white" type="file" name="image" required>
        </div>

        <div style="padding-bottom:15px;">
            <label >Description</label> <br>
            <textarea style="color:black" type="text" name="description" placeholder="description/caption" required rows="4" cols="50"></textarea>
            
        </div>

        <div style="margin-left:80px;padding-top:15px;">
            <input style="background-color:#3483eb;color:white;padding: 8px;" type="submit" value="Add New Product">
            <input style="background-color:#28bd57;color:white;padding: 8px;" type="reset" value="Reset the form">
        </div>


    </form>


    <div>

      <table bgcolor="white" style="color:black;border-color:black;" >
        <tr align="center">
          <th style="padding: 30px;">Food Name</th>
          <th style="padding: 30px;">Price</th>
          <th style="padding: 30px;">Description</th>
          <th style="padding: 30px;">Image</th>
          <th style="padding: 30px;" colspan="3">Action</th>
        </tr>

        @foreach($data as $data)

        <tr align="left">

          <td style="width: 100px;padding: 20px;">{{$data->title}}</td>
          <td style="width: 100px;padding: 20px;">${{$data->price}}</td>
          <td style="width: 100px;padding: 20px;">{{$data->description}}</td>
          <td style="width: 70px;"><img src="/foodimage/{{$data->image}}"></td>

          <td style="color:white;padding: 20px;"><a style="border: 3px solid red;background-color: red;
  border-radius: 10px" href="{{url('/deletemenu',$data->id)}}">Delete</a></td>
          <td style="color:white;padding: 20px;"><a style="border: 3px solid green;background-color: green;
  border-radius: 10px" href="{{url('/updatemenu',$data->id)}}">Update</a></td>
        </tr>

        @endforeach

      </table>

      
    </div>
<br>
<br><br><br>





  </div>

    </div>

  @include("admin.adminscript") 
  </body>
</html>