

<!DOCTYPE html>
<html lang="en">
  <head>
  @include("admin.admincss")
  </head>
  
  <div style="position: relative; left: 85%">
  <x-app-layout>

</x-app-layout>  
</div>
<style>

table,tr {
  
  border-radius: 15px;
}
th{
 
  border-collapse: collapse;
  background-color: #7ba0db;

}

tr:hover {background-color: #7ba0db;}

a:hover{color:black}
</style>

  <body>

  <div class="container-scroller">
  @include("admin.navbar")



  <div style="position: relative;top: 70px;right: -150;">
    <table bgcolor="white" style="color:black;border-color:black;" >

        <tr bgcolor="black" border="1px" align="center" >

            <th style="padding: 30px;">Name</th>
            <th style="padding: 30px;" colspan="2">Email</th>
            <th style="padding: 30px;">Phone</th>
            <th style="padding: 30px;" colspan="2">Address</th>
            <th style="padding: 30px;">Date</th>
            <th style="padding: 30px;">Time</th>
            <th style="padding: 30px;" colspan="3">Message</th>

        </tr>

          @foreach($data as $data)

        <tr align="center">

            <td style="padding: 30px;">{{$data->name}}</td>
            <td style="padding: 30px;" colspan="2">{{$data->email}}</td>
            <td>{{$data->phone}}</td>
            <td style="padding: 30px;" colspan="2">{{$data->address}}</td>
            <td>{{$data->date}}</td>
            <td>{{$data->time}}</td>
            <td style="padding: 20px;" colspan="3">{{$data->message}}</td>

        </tr>

        @endforeach

    </table>
 </div>

  
  </div>

  @include("admin.adminscript") 
  </body>
</html>