<!-- ***** Chefs Area Starts ***** -->
<section class="section" id="chefs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 text-center">
                    <div class="section-heading">
                        <h6>Our Product List</h6>
                        <h2>We offer the best ingredients for you</h2>
                    </div>
                </div>
            </div>

        
            

            <div class="row">

            @foreach($data2 as $data2)

                <div class="col-lg-4" style="padding-bottom: 50px;">
                    <div class="chef-item" >
                        <div class="thumb">
                            <div class="overlay"></div>
                            <ul class="social-icons">
                                <li><a href="https://www.facebook.com/SariwaFoods"><i class="fa fa-facebook"></i></a></li>
                                
                                <li><a href="https://www.instagram.com/bosslady_maricel/?hl=en  "><i class="fa fa-instagram"></i></a></li>
                            </ul>
                            <img src="chefimage/{{($data2->image)}}" style="width: 300px;height: 280px;">
                        </div>
                        <div class="down-content">
                            <h4>{{$data2->name}}</h4>
                            <h4><p>Price:</p>${{$data2->price}}/kilo</h4>
                            <span>{{$data2->specialty}}</span>
                        </div>
                    </div>
                </div>


                @endforeach
               
                
            </div>
        </div>
    </section>
    <!-- ***** Chefs Area Ends ***** -->