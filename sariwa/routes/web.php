<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route:: get("/",[HomeController::class, "index" ]); //homeblade agad

Route:: get("/redirects",[HomeController::class, "redirects" ]); 

//user

Route:: get("/users",[AdminController::class, "user" ]); 

Route:: get("/deleteuser/{id}",[AdminController::class, "deleteuser" ]); 

Route:: post("/uploaduser",[AdminController::class, "uploaduser" ]);

//food menu

Route:: get("/foodmenu",[AdminController::class, "foodmenu" ]); 

Route:: post("/uploadfood",[AdminController::class, "upload" ]);

Route:: get("/deletemenu/{id}",[AdminController::class, "deletemenu" ]); 

Route:: get("/updatemenu/{id}",[AdminController::class, "updatemenu" ]); 
Route:: post("/update/{id}",[AdminController::class, "update" ]); 

//reservation

Route:: post("/reservation",[AdminController::class, "reservation" ]);

Route:: get("/viewreservation",[AdminController::class, "viewreservation" ]);

//chef
Route:: get("/viewproduct",[AdminController::class, "viewproduct" ]);
Route:: post("/uploadchef",[AdminController::class, "uploadchef" ]);

Route:: get("/updateproduct/{id}",[AdminController::class, "updateproduct" ]);
Route:: post("/updatefoodchef/{id}",[AdminController::class, "updatefoodchef" ]);

Route:: get("/deletechef/{id}",[AdminController::class, "deletechef" ]);






Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
