@extends('layouts.app')
@section('content')

 <style>
   body {
  font-family: Arial, Helvetica, sans-serif;
  margin: 0;
}

html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.pic{
  width:50%;
  margin-left:25%;
  margin-bottom:5%;
}


.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  margin: 8px;
}

.about-section {
  padding: 50px;
  text-align: center;
  background-color: #474e5d;
  color: white;
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}


.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}
 </style>
 
    <div class="about-section">
  <h1> Scattershots</h1>
  <p>rabbit,gameplay,people i like, and meeeeee.</p>
</div>

</div>
<div class="pic">
      <img src="1.jpg" style="width:100%">
</div>
<div class="pic">
      <img src="2.jpg" style="width:100%">
</div>
<div class="pic">
      <img src="3.jpg" style="width:100%">
</div><div class="pic">
      <img src="4.jpg" style="width:100%">
</div><div class="pic">
      <img src="5.jpg" style="width:100%">
</div><div class="pic">
      <img src="6.jpg" style="width:100%">
</div><div class="pic">
      <img src="7.jpg" style="width:100%">
</div><div class="pic">
      <img src="8.jpg" style="width:100%">
</div><div class="pic">
      <img src="9.jpg" style="width:100%">
</div><div class="pic">
      <img src="10.jpg" style="width:100%">
</div><div class="pic">
      <img src="11.jpg" style="width:100%">
</div><div class="pic">
      <img src="12.jpg" style="width:100%">
</div><div class="pic">
      <img src="12a.jpg" style="width:100%">
</div>

<h2 style="text-align:center">Character References</h2>
<div class="row">
  <div class="column">
    <div class="card">
      <img src="icons1.png" style="width:100%">
      <div class="container">
        <h2>Rosevie Aquino Velasco</h2>
        <p class="title">Scholarship Coordinator</p>
        <p>University of Pangasinan - PHINMA</p>
        <p>Arellano St,Dagupan City -2400</p>
</br>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="icons.png"  style="width:100%">
      <div class="container">
        <h2>Jomarc Stan Vidal</h2>
        <p class="title">Professor</p>
        <p>University of Pangasinan - PHINMA</p>
        <p>Arellano St,Dagupan City -2400</p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="icons.png" style="width:100%">
      <div class="container">
      <h2>Engilbert Comadre</h2>
        <p class="title">IT Professor</p>
        <p>University of Pangasinan - PHINMA</p>
        <p>Arellano St,Dagupan City -2400</p>
      </div>
    </div>
  </div>
</div>

@endsection
