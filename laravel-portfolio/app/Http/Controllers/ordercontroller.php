<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ordercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function item($itemnum,$itemname,$price)
    {
        return "itemnumber:".$itemnum."<br> ItemName:".$itemname. "<br> Price:".$price;
    }

    public function order($customerid,$Name,$OrderNo,$Date)
    {
        return "CUSTOMER ID:".$customerid."<br> NAME:".$Name."<br> ORDERNUMBER:".$OrderNo."<br> DATE:".$Date;
    }
    
    public function customer($customerid,$name,$address)
    {
        return "Customer id:".$customerid."<br> Name:".$name."<br> Address:".$address;
    }
    public function orderdetails($TransNo,$OrderNo,$Itemid,$Name,$Price,$Qty)
    {
        return "TransNumber:".$TransNo."<br> Name:".$Name."<br> OrderNumber:".$OrderNo."<br> Price:".$Price."<br> QTY:".$Qty;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
