<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Postscontroller;
use App\Http\Controllers\ordercontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');


});
Route::get('/aboutme', function () {
    return view('aboutme');


});
Route::get('/contact', function () {
    return view('contact');


});


Route::get('/item/{itemnum?}/{itemname?}/{price?}',[ordercontroller::class, 'item'])->name('item1');
Route::get('order/{customerid?}/{Name?}/{OderNo?}/{Date?}',[ordercontroller::class, 'order'])->name('oder1');
Route::get('/customer/{customerid?}/{name?}/{address?}',[ordercontroller::class, 'customer'])->name('customer1');
Route::get('orderdetails/{TransNo?}/{OrderNo?}/{Itemid?}/{Name?}/{Price?}/{Qty?}',[ordercontroller::class, 'orderdetails'])->name('orderdetails1');


// {
//     return "item number: ".$itemnum."<br>item name: ".$itemname."<br>price: ".$price."<br>DemiSabiano/BSIT3C";


// });
// Route::get('customer/{id?}/{name?}/{age?}', function ($id='20UR0012',$name = 'demi',$age = '22') 
// {
//     return "id number: ".$id."<br>name: ".$name."<br>age: ".$age."<br>DemiSabiano/BSIT3C";


// });
// Route::get('order/{customerid?}/{customername?}/{ordernum?}', function ($customerid='004',$customername='bruce wayne',$ordernum='003') 
// {
//     return "customer id : ".$customerid."<br>customer name: ".$customername."<br>order number: ".$ordernum."<br>DemiSabiano/BSIT3C";


// });
// Route::get('orderdetails/{trans?}/{ordernumber?}/{itemid?}/{cusname?}/{pricee?}/{qty?}', function ($trans='001',$ordernumber='003',$itemid='003',$cusname='bruce wayne',$pricee='1,800',$qty='1') 
// {
//     return "trans number: ".$trans."<br>order number: ".$ordernumber."<br>item id: ".$itemid."<br>customer name: ".$cusname."<br>price: ".$pricee."<br>Quantity: ".$qty."<br>DemiSabiano/BSIT3C";


// });
//Route::get('student/details', function () {
 //   $url=route('student.details');
 //   return $url;
//})->name('student.details');

//Route::resource('/contact', Postscontroller::class,); 
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/customer',[OrderController::class, 'customers']);
Route::get('/order',[OrderController::class, 'orders']);
Route::get('/orderdetails',[OrderController::class, 'orderdetail']);
Route::get('/',[OrderController::class, 'item']);
//Route::get('/aboutme', [App\Http\Controllers\HomeController::class, 'aboutme'])->name('aboutme');
//Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');






